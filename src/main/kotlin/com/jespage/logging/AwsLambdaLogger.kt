package com.jespage.logging

import org.slf4j.LoggerFactory
import org.slf4j.Marker
import org.slf4j.event.Level
import org.slf4j.helpers.AbstractLogger
import org.slf4j.helpers.MessageFormatter
import org.slf4j.spi.LocationAwareLogger.*
import java.util.*

class AwsLambdaLogger(
    name: String,
    level: Level,
    var shortName: Boolean
) : AbstractLogger() {
    companion object {
        private val IS_LAMBDA = "AWS_EXECUTION_ENV" in System.getenv()
        private const val MIN_CAPACITY = 32
        private val timeZone = TimeZone.getDefault()
        private val locale = Locale.getDefault(Locale.Category.FORMAT)
        private val outStream = System.out
        private val writeMutex = Unit
        var awsRequestId: String? = null
        val loggerFactory = LoggerFactory.getILoggerFactory() as AwsLambdaLoggerFactory

        private fun Level.toIntSafe() = when (this) {
            Level.ERROR -> ERROR_INT
            Level.WARN -> WARN_INT
            Level.INFO -> INFO_INT
            Level.DEBUG -> DEBUG_INT
            Level.TRACE -> TRACE_INT
        }
    }

    init {
        this.name = name
    }

    private var levelInt = level.toIntSafe()
    var level
        get() = when (levelInt) {
            ERROR_INT -> Level.ERROR
            WARN_INT -> Level.WARN
            INFO_INT -> Level.INFO
            DEBUG_INT -> Level.DEBUG
            else -> Level.TRACE
        }
        set(value) {
            levelInt = value.toIntSafe()
        }

    override fun isTraceEnabled() = TRACE_INT >= levelInt
    override fun isTraceEnabled(marker: Marker?) = isTraceEnabled()
    override fun isDebugEnabled(): Boolean = DEBUG_INT >= levelInt
    override fun isDebugEnabled(marker: Marker?) = isDebugEnabled()
    override fun isInfoEnabled(): Boolean = INFO_INT >= levelInt
    override fun isInfoEnabled(marker: Marker?) = isInfoEnabled()
    override fun isWarnEnabled(): Boolean = WARN_INT >= levelInt
    override fun isWarnEnabled(marker: Marker?) = isWarnEnabled()
    override fun isErrorEnabled(): Boolean = ERROR_INT >= levelInt
    override fun isErrorEnabled(marker: Marker?) = isErrorEnabled()
    override fun getFullyQualifiedCallerName(): String? = null

    override fun handleNormalizedLoggingCall(
        level: Level,
        marker: Marker?,
        messagePattern: String?,
        arguments: Array<out Any>?,
        throwable: Throwable?
    ) {
        val s = buildString(MIN_CAPACITY) {
            if (!IS_LAMBDA) appendTime().append(' ')
            if (IS_LAMBDA) appendRequestId().append(' ')
            appendThreadName().append(' ')
            appendLevel(level).append(' ')
            appendName().append(" - ")
            if (marker != null) append(' ').append(marker.name).append(' ')
            append(MessageFormatter.basicArrayFormat(messagePattern, arguments))
        }
        synchronized(writeMutex) {
            outStream.println(s)
            throwable?.printStackTrace(outStream)
            outStream.flush()
        }
    }

    private val calendar = Calendar.getInstance(timeZone, locale)
    private fun StringBuilder.appendTime(): StringBuilder {
        calendar.timeInMillis = System.currentTimeMillis()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        val seconds = calendar.get(Calendar.SECOND)
        val millisecond = calendar.get(Calendar.MILLISECOND)

        if (hour < 10) append('0')
        append(hour)
        append(':')
        if (minute < 10) append('0')
        append(minute)
        append(':')
        if (seconds < 10) append('0')
        append(seconds)
        append('.')
        if (millisecond < 10) append('0')
        if (millisecond < 100) append('0')
        append(millisecond)
        return this
    }

    private fun StringBuilder.appendRequestId(): StringBuilder {
        append('<')
        awsRequestId?.let { append(it) }
        append('>')
        return this
    }

    private fun StringBuilder.appendThreadName(): StringBuilder {
        append('[')
        append(Thread.currentThread().name)
        append(']')
        return this
    }

    private fun StringBuilder.appendLevel(level: Level): StringBuilder {
        when (level) {
            Level.ERROR -> append("ERROR")
            Level.WARN -> append("WARN ")
            Level.INFO -> append("INFO ")
            Level.DEBUG -> append("DEBUG")
            Level.TRACE -> append("TRACE")
        }
        return this
    }

    private fun StringBuilder.appendName(): StringBuilder {
        if (shortName) {
            append(name, name.lastIndexOf('.') + 1, name.length)
        } else {
            append(name)
        }
        return this
    }
}