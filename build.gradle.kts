plugins {
    kotlin("jvm") version "1.9.23"
    `java-library`
    `maven-publish`
}

group = "com.jespage"
val artifactName = "aws-lambda-logging"
version = "2.0.3"

repositories { mavenCentral() }

java {
    withJavadocJar()
    withSourcesJar()
}

kotlin {
    jvmToolchain(11)
}

dependencies {
    api("org.slf4j:slf4j-api:2.0.13")
}

tasks.jar {
    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to artifactName,
                "Implementation-Version" to project.version
            )
        )
    }
}

publishing {
    repositories {
        maven {
            val projectId = System.getenv("CI_PROJECT_ID")
            url = uri("https://gitlab.com/api/v4/projects/${projectId}/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
    publications {
        create<MavenPublication>("library") {
            artifactId = artifactName
            from(components["java"])
        }
    }
}
