package com.jespage.logging

import org.slf4j.LoggerFactory
import org.slf4j.event.Level

fun main() {
    val logger = LoggerFactory.getLogger("com.jespage.logging.Test")
    logger.info("Testing short...")
    AwsLambdaLogger.loggerFactory.shortName = false
    logger.info("Testing long...")
    AwsLambdaLogger.loggerFactory.level = Level.INFO
    logger.debug("This message should not show.")
    logger.error("Logging a test exception", Exception("Test exception"))
}