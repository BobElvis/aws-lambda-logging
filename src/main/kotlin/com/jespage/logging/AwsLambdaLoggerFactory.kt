package com.jespage.logging

import org.slf4j.ILoggerFactory
import org.slf4j.Logger
import org.slf4j.event.Level
import java.util.concurrent.ConcurrentHashMap
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty

class AwsLambdaLoggerFactory : ILoggerFactory {
    companion object {
        val DEFAULT_LEVEL = Level.INFO
        const val DEFAULT_SHORT_NAME = true
    }

    var level by LoggerDelegate(DEFAULT_LEVEL, AwsLambdaLogger::level)
    var shortName by LoggerDelegate(DEFAULT_SHORT_NAME, AwsLambdaLogger::shortName)

    private val loggerMap = ConcurrentHashMap<String, AwsLambdaLogger>(16)
    override fun getLogger(name: String): Logger = loggerMap.computeIfAbsent(name, ::createLogger)
    private fun createLogger(name: String) = AwsLambdaLogger(name, level, shortName)

    inner class LoggerDelegate<T>(
        private var value: T,
        private val loggerProperty: KMutableProperty1<AwsLambdaLogger, T>
    ) : ReadWriteProperty<Any?, T> {
        override operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value
        override operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
            this.value = value
            loggerMap.forEach { loggerProperty.set(it.value, value) }
        }
    }
}